var assert = require('assert');
var server = require('../server');

describe('metaTemplate',function(){
  it('should return stringified JSON Object with properties "title" and "contributors"', function(){
    assert.strictEqual(
      '{"title":"test","contributors":["testerson"]}',server.metaTemplate({title:'test',creator:'testerson'})
    )
  })
})
