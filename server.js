var EPub = require('epub');
var fs = require('fs');

var filesArray = [];
var jsonArray = [];

fs.readdir("testepubs",function(err,files){
  if(err){return(err)};
  filesArray = files;

  files.forEach(function(file){
    var epub = new EPub("testepubs/"+file)
    epub.parse();
    epub.on("end", function(){
      fs.mkdir(file.split('.')[0],function(err,data){
        var dir = __dirname+'/'+file.split('.')[0]+'/index.json';
        fs.writeFile(dir,metaTemplate(epub.metadata));
      })
    })
  })
})

function metaTemplate(metadata){
  return JSON.stringify({
    title: metadata.title,
    contributors:[metadata.creator]
  })
}

var epub1 = new EPub("testepubs/5.epub")
epub1.parse();
epub1.on("end", function(){
  console.log(epub1.metadata)
})



module.exports = {
  metaTemplate,
}
